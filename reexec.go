package reexec

import (
	"os"
)

var reexecBindingFns = make(map[string]func())

// Register registers a new reexec "main" func binding under the specified name
func Register(name string, newMain func()) {
	reexecBindingFns[name] = newMain
}

// IsReexec checks if we are in reexec context (based on `os.Args[0]`)
func IsReexec() bool {
	return registered(os.Args[0])
}

// Run runs the registered "main" function binding
func Run() {
	fn, exists := reexecBindingFns[os.Args[0]]
	if exists {
		fn()
	}
}

func registered(binding string) bool {
	_, exists := reexecBindingFns[binding]
	return exists
}
