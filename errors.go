package reexec

import "errors"

var (
	// ErrPlatform is returned when the reexec package is used on an unsupported platform.
	ErrPlatform = errors.New("reexec: platform is not supported by this package")
	// ErrNotRegistered is returned when the reexec binding is not registered, but the command is being created.
	ErrNotRegistered = errors.New("reexec: binding is not registered")
	// ErrNoArgs command is called with no args.
	ErrNoArgs = errors.New("reexec: no args")
)
