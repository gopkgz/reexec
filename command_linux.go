package reexec

import (
	"os/exec"
	"syscall"
)

const (
	self = "/proc/self/exe"

	// SIGKILL kill -9
	SIGKILL = syscall.Signal(0x9) // "golang.org/x/sys/unix"
)

// Command creates exec.Cmd, that is ready to execute /proc/self/exe with the correct reexecBinding.
func Command(reexecBinding string, args ...string) (*exec.Cmd, error) {
	if len(args) == 0 {
		return nil, ErrNoArgs
	}
	if !registered(reexecBinding) {
		return nil, ErrNotRegistered
	}

	cmdArgs := []string{reexecBinding}
	cmdArgs = append(cmdArgs, args...)

	return &exec.Cmd{
		Path: self,
		Args: cmdArgs,
		SysProcAttr: &syscall.SysProcAttr{
			Pdeathsig: SIGKILL, // kill the process if the parent dies
		},
	}, nil
}
