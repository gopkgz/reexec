//go:build !linux
// +build !linux

package reexec

import (
	"os/exec"
)

// Command is not supported on this platform yet.
func Command(args ...string) (*exec.Cmd, error) {
	return nil, ErrPlatform
}
