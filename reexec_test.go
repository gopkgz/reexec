package reexec

import (
	"os"
	"os/exec"
	"strings"
	"testing"
)

const (
	testReexecBinding         = "reexecTest"
	testReexecNewMainExitCode = 42
)

func init() {
	Register(testReexecBinding, testReexecNewMain)

	if IsReexec() {
		Run()
	}
}

func testReexecNewMain() {
	os.Exit(testReexecNewMainExitCode)
}

func TestRegister(t *testing.T) {
	binding := "reexec" + t.Name()

	Register(binding, func() {
		panic("Return Error")
	})
	_, ok := reexecBindingFns[binding]
	if !ok {
		t.Errorf("Register() failed")
	}
}

func TestIsReexecInNormalContext(t *testing.T) {
	if IsReexec() {
		t.Error("IsReexec() should return false")
	}
}

func TestReexecRun(t *testing.T) {
	Register(testReexecBinding, testReexecNewMain)

	cmd, err := Command(testReexecBinding, "/bin/sh", "-c", "echo $0")
	if err != nil {
		t.Errorf("Command() creation failed: %v", err)
	}
	if err := cmd.Run(); err != nil {
		if exitErr, ok := err.(*exec.ExitError); ok && exitErr.ExitCode() != testReexecNewMainExitCode {
			t.Errorf("cmd.Run() failed: %v", err)
		}
	}
}

func TestCommand(t *testing.T) {
	expectedCmd := "/bin/sh -c echo $0"

	cmd, err := Command(testReexecBinding, strings.Split(expectedCmd, " ")...)
	if err != nil {
		t.Errorf("Command() creation failed: %v", err)
	}
	if cmd.Path != "/proc/self/exe" {
		t.Errorf("Command() Path is not /proc/self/exe")
	}
	if cmd.Args[0] != testReexecBinding {
		t.Errorf("Command() Args[0] is not %s", testReexecBinding)
	}
	args := strings.Join(cmd.Args[1:], " ")
	if args != expectedCmd {
		t.Errorf("Command() Args is %s, expected %s", args, expectedCmd)
	}
}
